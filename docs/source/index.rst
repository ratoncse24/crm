==========
Django CRM
==========

Django CRM is an opensource CRM on Django framework developed and maintained by `ratoncse24`_. It has all the basic features of Sales and Marketing to begin with and customise to meet industry needs.

.. _ratoncse24: https://ratoncse24.com/

We welcome code contributions, suggestions, and feature requests via github. Source Code is available in `ratoncse24 Repository`_.

.. _ratoncse24 Repository: https://github.com/ratoncse24/Django-CRM.git

Installation
************
.. toctree::
   :maxdepth: 2

   setup/index.rst
   setup/packages_used.rst

Modules in Sales
****************

.. toctree::
  :maxdepth: 1

  apps/crm/dashboard.rst
  apps/crm/accounts.rst
  apps/crm/contacts.rst
  apps/crm/leads.rst
  apps/crm/opportunity.rst
  apps/crm/cases.rst
  apps/crm/documents.rst
  apps/crm/tasks.rst
  apps/crm/invoices.rst
  apps/crm/events.rst
  apps/crm/teams.rst

Modules in Marketing
********************

.. toctree::
  :maxdepth: 1

  apps/marketing/dashboard.rst
  apps/marketing/contact-list.rst
  apps/marketing/contacts.rst
  apps/marketing/email-templates.rst
  apps/marketing/campaign.rst

Internals
*********

.. toctree::
  :maxdepth: 1

  apps/internals/users.rst
  apps/internals/settings.rst
  apps/internals/profile.rst
  apps/internals/change_password
  apps/internals/forgot_password



We welcome your feedback and support, raise github ticket if you want to report a bug or need new feature.

If you need any additional support? `Contact us here`_

.. _contact us here: https://ratoncse24.com/contact-us/

    or

mailto:: "hello@ratoncse24.com"
